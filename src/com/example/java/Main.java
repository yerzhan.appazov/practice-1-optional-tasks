package com.example.java;

public class Main {

    public static void main(String[] args) {
        //  Max and min values from fixed array (Task 2.1)
        int[] arrayOfIntegers = {0, 1, 1, 2, 3, 5, 6, 7, 8};
        System.out.println(minAndMax(arrayOfIntegers));

        // inverse of a string
        System.out.println(reverseString("ABCDEF"));
    }

    //  Max and min values from fixed array (Task 2.1)
    public static String minAndMax(int[] arrayOfIntegers) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int number :
                arrayOfIntegers) {
            if (number < min) min = number;
            if (number > max) max = number;
        }
        return "Min: " + min +
                "\nMax: " + max;
    }
    public static String reverseString(String inputString) {
    String result = "";
    for(int i = inputString.length() - 1; i>=0; i--) {
        result += inputString.charAt(i);
    }
        return result;
    }
}
